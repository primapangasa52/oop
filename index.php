<?php
require('animal.php');
require('Ape.php');
require('Frog.php');


$sheep = new Animal("shaun");

echo "Nama Hewan: " . $sheep->name; // "shaun"
echo "<br>";
echo "Jumlah Kaki: " . $sheep->legs; // 4
echo "<br>";
echo "Apakah termasuk hewan berdarah dingin? " . $sheep->cold_blooded; // "no"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama Hewan: " . $kodok->name;
echo "<br>";
echo "Jumlah Kaki: " . $kodok->legs; // 4
echo "<br>";
echo "Apakah termasuk hewan berdarah dingin? " . $kodok->cold_blooded; // "no"
echo "<br>";
echo "Lompatan Hewan Tersebut: ";
$kodok->jump(); // "Hop Hop"
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan: " . $sungokong->name;
echo "<br>";
echo "Jumlah Kaki: " . $sungokong->legs; // 4
echo "<br>";
echo "Apakah termasuk hewan berdarah dingin? " . $sungokong->cold_blooded; // "no"
echo "<br>";
echo "Suara Hewan Tersebut: ";
$sungokong->yell(); // "Auooo"
